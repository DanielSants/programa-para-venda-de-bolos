        /* ------------------------------- */
        /* UNIVERSIDADE FEDERAL DE SERGIPE */
        /* CAMPUS PROF... ALBERTO CARVALHO */
        /* PROJETO.... TRAINEE ITATECH JR. */
        /* ALUNO........ DANIEL DOS SANTOS */
        /* ------------------------------- */

    /* Programa para auxiliar na venda de bolos */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

int main(void)
{
    int opc, qtdeBolos, qtdeBolosV, qtdeBolosAdd;
    char opc2, confirma;

    struct cadsBolos
    {
        char adcod[11], desc[11], cod[11], codVenda[11];
        float preco, pesoGramas, custo=0;
        int quantidade, qtdeVenda, newQuant;
    };
    struct cadsBolos cadsBolos[11];
    int indice;

    //Primeira parte do programa: "Menu"
    do
    {
        printf("    =-------------=\n");
        printf("    Seja bem vindo!\n");
        printf("    =-------------=\n");
        printf("Programa para auxiliar na venda de bolos!\n\n");
        printf("        Menu:\n\n");
        printf("1 - Cadastrar novo bolo;\n");
        printf("2 - Adicionar bolos;\n");
        printf("3 - Listar bolos dispo%cneis;\n",141);
        printf("4 - Iniciar uma venda;\n");
        printf("5 - Faturamento/ Extrato de vendas;\n");
        printf("6 - Sair;\n\n");
        printf("Digite uma op%c%co: ",135,198);
        scanf("%d",&opc);
        //Segunda parte
        system("cls");
        switch(opc)
        {
                //Gardar as informações de cada bolo.
            case 1: printf("    1 - Cadastrar novo bolo: \n\n");
                printf("Ir%c cadastar quantos novos bolos? ",160);
                scanf("%d",&qtdeBolos);
                for(indice=1; indice<=qtdeBolos; indice++)
                {
                    fflush(stdin);
                    printf("Insira o c%cdigo alfanum%crico do %d%c bolo: ",162,130,indice,167);
                    gets(cadsBolos[indice].adcod);
                    printf("Descreva o bolo/informe o nome popular dele: ");
                    gets(cadsBolos[indice].desc);
                    printf("Digite o pre%co: ",135);
                    scanf("%f",&cadsBolos[indice].preco);
                    while(cadsBolos[indice].preco <= 0)
                    {
                        printf("Pre%co inv%clido!\n",135,160);
                        printf("Digite um pre%co v%clido: ",135,160);
                        scanf("%f",&cadsBolos[indice].preco);
                    }
                    printf("Digite o peso em gramas do bolo de %s: ",cadsBolos[indice].desc);
                    scanf("%f",&cadsBolos[indice].pesoGramas);
                }break;
                printf("Cadastros realizados com sucesso!\n");

                //Informar a quantidade em estoque
            case 2: printf("2 - Adicionar quantidade de bolos dispon%cveis: \n\n",141);
                printf("Qual a quantidade de diferentes bolos que vai adicionar? ");
                scanf("%d",&qtdeBolosAdd);
                for(indice=1; indice<=qtdeBolosAdd; indice++)
                {
                    fflush(stdin);
                    printf("Digite o codigo do bolo que j%c estar cadastrado: ",160);
                    gets(cadsBolos[indice].cod);
                    if(strcmp(cadsBolos[indice].cod,cadsBolos[indice].adcod)==0)
                    {
                        printf("Digite a quantidade do bolo de %s para informar diponibilidade: ",cadsBolos[indice].desc);
                        scanf("%d",&cadsBolos[indice].quantidade);
                    }else{
                        printf("C%cdigo n%co cadastrado no sistema, volte ao menu principal\n",162,198);
                        printf("se deseja cadastrar um novo tipo de bolo.\n");
                    }
                }break;
                printf("Estoque/Disponibilidade atualizada!\n");

                //Exibir lista completa
            case 3: printf("3 - Listar bolos dispo%cneis;\n\n",141);
                printf("Cod.    Quantidade.     Descricao.      Preco.      Peso.\n");
                for(indice=1; indice<=qtdeBolos; indice++)
                {
                    printf("%s %8d          %5s %16.2f %10.2f\n",cadsBolos[indice].adcod,cadsBolos[indice].quantidade,cadsBolos[indice].desc,cadsBolos[indice].preco,cadsBolos[indice].pesoGramas);
                }break;

                //Iniciar um processo de venda
            case 4: printf("4 - Iniciar uma venda;\n\n");
                printf("Qual a quantidade de diferentes bolos que vai vender? ");
                scanf("%d",&qtdeBolosV);
                for(indice=1; indice<=qtdeBolosV; indice++)
                {
                    fflush(stdin);
                    printf("Digite o codigo do bolo: ");
                    gets(cadsBolos[indice].codVenda);
                    if(strcmp(cadsBolos[indice].codVenda,cadsBolos[indice].cod)==0)
                    {
                        printf("Quantos bolos de %s vai vender: ",cadsBolos[indice].desc);
                        scanf("%d",&cadsBolos[indice].qtdeVenda);
                    }
                        if(cadsBolos[indice].qtdeVenda > cadsBolos[indice].quantidade)
                        {
                            printf("N%co %c poss%cvel proseguir com a venda, pois essa quantidade %c superior a que h%c no estoque.",198,130,141,130,160);
                        }
                        else
                        {
                            cadsBolos[indice].custo=0;
                            cadsBolos[indice].custo=(cadsBolos[indice].preco * cadsBolos[indice].qtdeVenda);
                            printf("Essa compra custa: %5.2f\n",cadsBolos[indice].custo);
                            getchar();
                            printf("Confirma? s/n ");
                            confirma=getchar();
                        }
                            if(confirma=='S'||confirma=='s')
                            {
                                printf("Venda realizada com sucesso!\n");
                                cadsBolos[indice].newQuant = cadsBolos[indice].quantidade - cadsBolos[indice].qtdeVenda;
                            }
                }break;

                //Listar o faturamento a partir das vendas.
            case 5: printf("Cod.    Em Estoque.     Vendidos.       Faturamento.\n");
                for(indice=1; indice<=qtdeBolos; indice++)
                {
                    printf("%s  %8d  %12d  %15.2f\n",cadsBolos[indice].adcod,cadsBolos[indice].newQuant,cadsBolos[indice].qtdeVenda,cadsBolos[indice].custo);
                }break;
            //Encerrar
            case 6: printf("Obrigado!\n"); break;
            default : printf("Op%c%co inv%clida!\n",135,198,160);
        }
        getchar();
        printf("\nDeseja encerrar? s/n\n");
        opc2=getchar();
        system("cls");
    }while(opc2=='n'||opc2=='N');
    return 0;
}
